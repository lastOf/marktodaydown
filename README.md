# MarkTodayDown

## Description

MarkTodayDown is a binary written in Rust to generate a Markdown file in the current directory with the current date leading the title. I put this together initially as a Deno script to simplify my notetaking (so that I didnt have to identify the date or format it correctly in order to start a new note). Then I realized it would be a great chance to write something useful in Rust that is within reach 😆.

It definitely took me longer to write in Rust, I had to look up a good bit of syntax including how to change a String into a &u8, `format!()` for generating a templated string, Chrono for getting the current date, `to_string()` for changing the date into a string, vector methods like `remove(0)` to remove the first element, and `join("-")` for combining the vector elements into a string with a dash between each element.

The same program in Deno compiles to a 52MB binary vs the 415KB binary from the rust version.

### Dependencies

- Chrono

## Useage

`cargo build --release`

The binary will be located in ./target/release/today

To run the binary enter `./today` into the terminal from the directory containing the the binary.

## License

MIT
