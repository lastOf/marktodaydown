use chrono;
use chrono::Datelike;
use std::env;
use std::fs::File;
use std::fs::OpenOptions;
use std::io::prelude::*;

fn main() -> std::io::Result<()> {
    let mut args: Vec<String> = env::args().collect();
    let path = env::current_dir()?;
    let current_direct = path.to_str().unwrap().split('/').last().unwrap();
    let mut timeline_file_loc = "./".to_string();
    timeline_file_loc.push_str(&current_direct);
    timeline_file_loc.push_str("-timeline.md");
    let mut time_string = "".to_string();
    if !std::path::Path::new(&timeline_file_loc).exists() {
        time_string.push_str("# Timeline for ");
        time_string.push_str(&current_direct);
        time_string.push_str(
            "
```
|
|
|",
        );
    }

    let current_time = chrono::offset::Local::now();
    let mut ss: String = current_time.to_string()[2..10].to_string();
    let weekday = current_time.date().weekday();
    args.remove(0);
    if args.len() > 0 {
        ss = ss + "-"
    }
    let title: String = ss + &args.join("-") + ".md";
    let mut file = File::create(&title).unwrap();

    time_string.push_str(
        "
|
|
|",
    );
    time_string.push_str("_____ ");
    time_string.push_str(&title);

    let mut timeline_file = OpenOptions::new()
        .create(true)
        .append(true)
        .open(&timeline_file_loc)?;
    let timeline_content = &time_string.as_bytes();

    timeline_file.write_all(&timeline_content)?;

    // println!("The current directory is {}", timeline_file);
    // check if current directory has a file named dir-timeline

    //if so read to string

    //if not make that folder and append

    let formatted_string = format!(
        "# {title}

*{weekday}, {date}*

## Event

## Reflections

## Plans",
        title = &args.join(" "),
        date = chrono::offset::Local::now().to_string(),
        weekday = weekday
    );
    let content = formatted_string.as_bytes();
    file.write_all(content)?;
    println!("Created file named: {:?}", title);
    Ok(())
}
